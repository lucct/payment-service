provider "aws" {
  region     = "${var.aws-region}"
  version    = "~> 2.0"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
}


resource "aws_key_pair" "thachnn" {
  key_name   = "thachnn"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQChqUReLy/YYeaQI9mKozTqsnD9+3j6Ou40TOyY6YDp2+LNanmML7wRmI4U76Kg5WD7hhdODSVdcBNB5WF8iV7oJCK1yPBl9OMslBy7g+Gp308XP/OE3+6g9kXSQjKx8AdfOd/k9WPtyrzRBh4VEvDFV7oohTKmKT9F8wWiojsA6w3QksCpsEqvuOVEd7qdamZYJ1kS5KYUkgrGOxkKL07Uty75fdNlb1P0x7fASHPjQ+VX+9ZLnv6ebdy0qHrUOTBc4N8zo3fDaYYujKnuJHWCfKQ5lZY7uMMmXMw2MpLe83tqEDuc+2RMB8ZZacJGCY6aYAFK/B1x72VgJY9BAlqRFGc1aVmmH6h0aNpJ8qaj17AE8fvWx32TLDs06PoxJz4/67CbkAAy+pvx2vtvcDPgzaHwTBrmjEdU/9XnWtAFfwDaioQTGigMwmdDSaLdgwPD4uwEjYNI+y1WRgZtwQWIGd8eHyxZakdHdYvxw/plFeH9HOhIUSsgs0X0aXYUPkOUccryrYxiqgHYE42FUa8IJp4mH8iJkO6aGae/RpnABTdYUey80JlFuErWzvStVBvysvABIHs+VKhvq6bjxQdzHoc3h6pJBnW8akQll9kv7762B953bkNfCdDhNnz5RPcd40wBXOBUAaMvOM6X/tbJyuaUqcKA5maByAo01gTZsQ== thachnguyen@s-mis-thachnguyen.local"
}

resource "aws_instance" "lb" {
  ami           = "ami-0b4dd9d65556cac22"
  instance_type = "t2.nano"
  key_name      = aws_key_pair.thachnn.key_name
  user_data     = file("nginx_loadbalance.sh")
  network_interface {
    network_interface_id = aws_network_interface.lb.id
    device_index         = 0
  }
  tags = {
    Name  = "Nginx"
  }
}
resource "aws_eip_association" "eip_assoc" {
  instance_id   = aws_instance.lb.id
  allocation_id = aws_eip.lb.id
}

resource "aws_eip_association" "payment_service" {
  instance_id   = aws_instance.payment_service.id
  allocation_id = aws_eip.payment_service.id
}
resource "aws_eip_association" "order_service" {
  instance_id   = aws_instance.order_service.id
  allocation_id = aws_eip.order_service.id
}

resource "aws_eip" "lb" {
  vpc = true
}
resource "aws_eip" "order_service" {
  vpc = true
}
resource "aws_eip" "payment_service" {
  vpc = true
}
resource "aws_instance" "order_service" {
  ami           = "ami-0b4dd9d65556cac22"
  instance_type = "t2.nano"
  key_name      = aws_key_pair.thachnn.key_name
  user_data     = file("orderservice.sh")
  network_interface {
    network_interface_id = aws_network_interface.order.id
    device_index         = 0
  }
  tags = {
    Name  = "order_service"
  }
}
resource "aws_instance" "payment_service" {
  ami           = "ami-0b4dd9d65556cac22"
  instance_type = "t2.nano"
  key_name      = aws_key_pair.thachnn.key_name
  user_data     = file("paymentservice.sh")
  network_interface {
    network_interface_id = aws_network_interface.payment.id
    device_index         = 0
  }
  tags = {
    Name  = "payment_service"
  }
}
resource "aws_security_group" "elb_http" {
  name        = "elb_http"
  description = "Allow HTTP traffic to instances through Elastic Load Balancer"
  vpc_id = aws_vpc.microservices.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP through ELB Security Group"
  }
}

resource "aws_elb" "web_elb" {
  name = "web-elb"
  security_groups = [
    aws_security_group.elb_http.id
  ]

  subnets = [
    aws_subnet.microservices-subnet-public.id
  ]

  cross_zone_load_balancing   = true

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }

  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = 80
    instance_protocol = "http"
  }

}

#Adding Elastic IP for NAT gateway

resource "aws_eip" "nat_gateway" {
  vpc = true
}

#Adding NAT Gateway

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.nat_gateway.id}"
  subnet_id     = "${aws_subnet.microservices-subnet-private-1.id}"
}
