#!/bin/bash
setenforce 0
yum remove -y firewalld
yum install epel-release -y
curl -sL https://rpm.nodesource.com/setup_10.x | bash -
sudo yum clean all && sudo yum makecache fast
sudo yum install -y gcc-c++ make git
sudo yum install -y nodejs
mkdir /nodeapp && mkdir /root/.ssh/
cd /nodeapp
npm install --save express
cat > /root/.ssh/id_rsa <<EOF
-----BEGIN RSA PRIVATE KEY-----
MIIEowIBAAKCAQEAt6wOdYs/y+JCwtcPOsyarpOhM7Dt5wrkJBTWiQMuX+Q2kGTV
MrLIbcvHMuT0LHyTapLZKm2F7XcSy6jFP/wMQWdxiuQkCCxkbgh7jfkhGPo+ZK3R
9h1bbg3VCkd0p3pbDvGFzx3MU1MkUWHa3R7aWzrl66jLN67nDcmtBURayOo7i7nw
CZieGEabaoi6s0CAHeYtvfYQNsmm0VYDPzqQdKqnvVHIalvslAN4lodJPpLSG9Fg
Ocb8jmMn/ny6aEOYT6/bgr519YFZ2mCrvBrPkMzvcRRDIU/PZCjhnSFMPx85qWNf
9hVPj3RwOAZKCf3PC6zpHHTMcP+N7wzuNyXzIwIDAQABAoIBACalB+HJa6HdOEjA
cQ7hD8adfzReDKApFXlV/RGU3sQEQ0q8PHNPVIZtcrsNmyZlC6tHvHOF2cDt5cF+
AAqHYJi+SBB+Eeq6VGMmW8CyYzlbJZePTvbmlF8if+mKwUufndsWDB5Y2a3Kd4C8
Aw21OSIq0Ud1G1Ta8vher43FJQjSjHFHodFE1lzjrT910hwkphqdOjKgyhjNMNKS
EnGypa3x104g2t4Y54pJikrPPOKq7GsdubA1oS50cT2954H8JESsoXnT/xASTbPV
ou7TGQ42Pyc+z0vlqPsJQOzsRPzCOWABLyZzmES45QRgJm3Wjr9L2D40FwIhFXIS
vw0cPRECgYEA3ct02c3Qs1e3t4IiBbg0RjQEXMNxjnPVtUnjtDDB117WZVyM48fK
//0k4AHm6xhJC1VM4mVMjzeVoaFz4gNoYyCy4WbuyVd5PDJgxNf0oa432Haq+FGi
8l6AZUXCMmTQGo7kFxY3ddzAx1iUGS5P07JrJhUYe8AI7hTSg3bSOQkCgYEA0/+A
riz0F1oFIaw6o/AsP0YvCKjGGp58IOmo++fkBlNO+Qa0bPGo0yHnvPGl/7CfmnC0
fK4dr2AEsEys9brD9YHZFkhXVae3UqiDXmOQUhBEOulxbrQuNi3sI6HHSti+4irO
ULKECRvHldaRkfl/AAxnN5BLMaCH+1cm679IMcsCgYB0BPuo+1qSle4KVk5Jw6PE
nW0ywSHjrIA8BtzP4zli2b844oLonBh6dAcYDqamj9uVzBcdznOP7zmchQJDVhkC
0ZYBBoEeXV2TuZ6ZGYZkFOgtmVWx87JTKk/rqeP9zHJ3kguKcBqu0r0dCh7eVTV4
vboC/8IA6eFB+S5tinhJUQKBgQCLV3D5CssVXjHpRA73GjG7FSJtCqHLdUgZSfus
N2sFTVSnEeUdhml0/OAYrKxzN2BwXowDN16DmxT53CwDB9MtVz+dCcd9oVM5nVFb
hQ2jItX18uJ/5SZKrjx73yXi6Dvu5DvfhraKVhwFPJdJ3ze+jGh7ePgzklROu6X1
ZjDJqQKBgEwnH/Fvwr7MVRUrQaJ/ZdoVXHncxYnRfsH85sY1tQZtYcRliEGkVQcE
qUjD81JkHsV5jbWae3dzgDD8XryPpJLM4bX+2vHHFkUriNrltfJrHFOL+FmxpGKG
CZOsF3zyms7CQdkbp69Gq/Z8wewcRFT+pk9RY2HRnpQKHIt/uE8S
-----END RSA PRIVATE KEY-----
EOF
echo -e "StrictHostKeyChecking no\n" >> ~/.ssh/config
curl --silent --location https://dl.yarnpkg.com/rpm/yarn.repo | sudo tee /etc/yum.repos.d/yarn.repo
sudo rpm --import https://dl.yarnpkg.com/rpm/pubkey.gpg
yum install yarn -y
chmod 400 /root/.ssh/id_rsa
git clone git@bitbucket.org:lucct/payment-service.git
cd /nodeapp/payment-service
sed -ie '/REDIS_URL/ s/localhost/10.0.1.10/' .env
yarn install
yarn build
yarn run start:prod & 


