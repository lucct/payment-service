resource "aws_db_subnet_group" "private" {
  name       = "microservices-db-subnet-group-private"
  subnet_ids = [aws_subnet.microservices-subnet-private-1.id, aws_subnet.microservices-subnet-private-2.id]

  tags = {
    Name = "Private DB Subnet Group"
  }
}

resource "aws_internet_gateway" "microservices" {
  vpc_id = aws_vpc.microservices.id
}

resource "aws_route_table" "allow-outgoing-access" {
  vpc_id = aws_vpc.microservices.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.microservices.id
  }

  tags = {
    Name = "Route Table Allowing Outgoing Access"
  }
}

resource "aws_route_table_association" "microservices-subnet-public" {
  subnet_id      = aws_subnet.microservices-subnet-public.id
  route_table_id = aws_route_table.allow-outgoing-access.id
}

resource "aws_route_table_association" "microservices-subnet-private-1" {
  subnet_id      = aws_subnet.microservices-subnet-private-1.id
  route_table_id = aws_route_table.allow-outgoing-access.id
}

resource "aws_security_group" "allow-internal-nodejs" {
  name        = "allow-internal-nodejs"
  description = "Allow internal nodejs requests"
  vpc_id      = aws_vpc.microservices.id

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.microservices.cidr_block]
  }
 ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.microservices.cidr_block]
  }
}

resource "aws_security_group" "allow-internal-mysql" {
  name        = "allow-internal-mysql"
  description = "Allow internal MySQL requests"
  vpc_id      = aws_vpc.microservices.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.microservices.cidr_block]
  }
}

resource "aws_security_group" "allow-http" {
  name        = "allow-http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = aws_vpc.microservices.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow-ssh" {
  name        = "allow-ssh"
  description = "Allow SSH inbound traffic"
  vpc_id      = aws_vpc.microservices.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow-all-outbound" {
  name        = "allow-all-outbound"
  description = "Allow all outbound traffic"
  vpc_id      = aws_vpc.microservices.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_subnet" "microservices-subnet-public" {
  availability_zone_id = "apse1-az1"
  cidr_block           = "10.0.0.0/24"
  vpc_id               = aws_vpc.microservices.id

  tags = {
    Name = "Microservices Subnet (Public)"
  }
}

resource "aws_subnet" "microservices-subnet-private-1" {
  availability_zone_id = "apse1-az1"
  cidr_block           = "10.0.1.0/24"
  vpc_id               = aws_vpc.microservices.id

  tags = {
    Name = "Microservices Subnet (Private 1)"
  }
}

resource "aws_subnet" "microservices-subnet-private-2" {
  availability_zone_id = "apse1-az2"
  cidr_block           = "10.0.2.0/24"
  vpc_id               = aws_vpc.microservices.id

  tags = {
    Name = "Microservices Subnet (Private 2)"
  }
}

resource "aws_vpc" "microservices" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "Microservices VPC"
  }
}
resource "aws_network_interface" "order" {
  security_groups = ["${aws_security_group.allow-internal-nodejs.id}", "${aws_security_group.allow-ssh.id}", "${aws_security_group.allow-all-outbound.id}"]
  subnet_id   = aws_subnet.microservices-subnet-private-1.id
  private_ips = ["10.0.1.10"]
}
resource "aws_network_interface" "payment" {
  security_groups = ["${aws_security_group.allow-internal-nodejs.id}", "${aws_security_group.allow-ssh.id}", "${aws_security_group.allow-all-outbound.id}"]
  subnet_id   = aws_subnet.microservices-subnet-private-1.id
  private_ips = ["10.0.1.11"]
}

resource "aws_network_interface" "lb" {
  security_groups = ["${aws_security_group.allow-http.id}", "${aws_security_group.allow-ssh.id}", "${aws_security_group.allow-all-outbound.id}"]
  subnet_id   = aws_subnet.microservices-subnet-public.id
  private_ips = ["10.0.0.9"]
}

data "aws_availability_zones" "all" {

}

