import { Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable } from 'typeorm';

export enum PaymentStatus {
    CONFIRMED = "CONFIRMED",
    DECLINED = "DECLINED"
}
@Entity()
export class Payment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  orderId: number;

  @Column()
  userId: number;

  @Column()
  totalAmount: number;

  @Column()
  status: PaymentStatus
}
