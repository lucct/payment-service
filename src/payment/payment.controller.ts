import { Controller } from '@nestjs/common';
import { PaymentService } from './payment.service';
import { MessagePattern } from '@nestjs/microservices';
import  {Payment, PaymentStatus} from './payment.entity' 
@Controller('payment')
export class PaymentController {
    constructor(
        private readonly paymentService: PaymentService
      ) {
  
    }

    @MessagePattern({ cmd: 'process_payment' })
    async processPayment(payload) {
        const {user, order, product, authPin} = payload;
        // We should store all these data in payment table in the real world
        
        if (user.auth && user.auth.pin !== authPin ||
            user.credits < order.totalAmount) {
            return PaymentStatus.DECLINED;
        }
        
        // Random status
        const enumValues = [PaymentStatus.CONFIRMED, PaymentStatus.DECLINED];
        const randomIndex = Math.floor(Math.random() * enumValues.length);
        console.log(enumValues, randomIndex, enumValues[randomIndex] );
        return enumValues[randomIndex];
    }
}
