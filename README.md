# Order Management System
2 small  microservices written in NestJS/NodeJS to demonstrate order management system.

#### Author comment
In the short term, I'm just really trying to demonstrate competence and the things I care about like scalability, pub-sub systems, coding standards, testing, performance, etc. in a single system as required. However, I have noted a few further developable steps below so that the system can be used if we continue to develop.
Best regards


## Tech Stack
- [Node](https://nodejs.org/en/download/)
- [NestJS](https://nestjs.com/)
- [NestJS Microservice](https://docs.nestjs.com/microservices/basics)
- [Redis](https://redis.io/)
- [SQLite](https://www.sqlite.org/index.html)
- [Jest](https://jestjs.io/)
- [TypeScript](https://www.typescriptlang.org/)
- [Terraform](https://www.terraform.io/)

## Design Structure
![structure](structure.png)

## AWS Terraform File
Can be found easily in ```deployment``` folder
- terraform init
- terrfform plan
- terrorm apply

## Setup

#### Clone the source locally
```
$ git clone https://bitbucket.org/lucct/payment-service
$ cd payment-service
```

#### Install project dependencies

Install root directory dependencies
```
$ yarn install
```
## Running the app

```bash
# Run Redis server as default
$ redis-server

# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```

## Test

```bash
# unit tests
$ yarn run test

# test coverage
$ yarn run test:cov
```

## Next steps
- Working with payment data
- Add more test cases
- Scalable Database and Redis
- Add logs
- Settting Auto Scaling group for high performance
- Redundant for databases group
- Using AWS Elastic Beanstalk for deployment microservice instead of EC2

## License

Nest is [MIT licensed](LICENSE).
